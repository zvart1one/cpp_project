#include <iostream>
#include <string>

class Animal
{
public:
	virtual void Voice() =  0;

private:

};

class Dog : public Animal 
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
	
private:

};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "May!" << std::endl;
	}
	
private:


};

class Bird : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Chirik!" << std::endl;
	}

	

private:


};

void main()
{
	Animal** animal_voice = new Animal * [3];
	animal_voice[0] = new Dog();
	animal_voice[1] = new Cat();
	animal_voice[2] = new Bird();
	for (int i = 0; i < 3; i++)
	{
		animal_voice[i]->Voice();
	}

}